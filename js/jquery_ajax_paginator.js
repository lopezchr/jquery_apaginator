(function($){

	//CLASE DE PAGINADOR
	var Paginator = function(element,params,callback){
		var currentPage = 1;
		var config = params;
		var container = element;
		var instance = this;
		var done = callback;

		this.getControl = function(){
			console.log(paginatorControl);
			//return $(paginatorControl).clone(true);
		}

		//FUNCIONES PUBLICAS CON ACCESO DESDE ELEMENT.DATA
		this.nextPage = function()
		{
			if(currentPage > 0){
				currentPage ++;
				this.changePage(currentPage);
			}
		};

		this.prevPage = function()
		{
			currentPage --;
			this.changePage(currentPage);
		};

		this.changePage = function(toPage)
		{
			currentPage = toPage;
			$(container).hide();
			//consulta a servidor
			$.post(config.source,{page:toPage,limit:config.limit},function(data){	

				//se borra el contenedor y todos los controles que contengan la clase especifica y la instancia
				$(container).html("");
				$('.ajax_paginator_control').each(function(index,paginatorElement){
					if($(paginatorElement).data('paginator') == instance){
						$(paginatorElement).remove();
					}
				});
				
				// ***** INICIO CONSTRUCCION DEL PAGINADOR *****
				//si es la primera pagina se desabilita el boton de pagina anterior
				if(currentPage == 1){	
					extra = 'class="disabled"';
				}
				var paginator = $('<ul class="pagination">');

				var goBack = $('<li '+extra+'><a>&laquo;</a></li>').on('click',function(){
					if(currentPage != 1){
						instance.prevPage();
					}
				});
				paginator.append(goBack);

				for(var page = 0;page < data.page_number;page ++){
					//secoloca la pagina actual como activa
					var extra = '';
					if ((page + 1) == currentPage) {
						extra = 'class="active"';
					}
					var item = $('<li '+extra+'><a>'+(page+1)+'</a></li>').on('click',function(){
						instance.changePage($(this).find("a").html());
					});
					paginator.append(item);
				}
				//si es la ultima pagina desabilita el boton de siguiente pagina
				var extra = '';
				if(currentPage == data.page_number){
					extra = 'class="disabled"';
				}
				var goNext = $('<li '+extra+'><a>&raquo;</a></li>').on('click',function(){
					if(currentPage < data.page_number){
						instance.nextPage();
					}
				});

				paginator.append(goNext);

				//se crea el control del paginador aÃ±adiendo clase especifica y datos de la instancia 
				var paginatorControl = $('<div class="col-xs-12 ajax_paginator_control">');
				$(paginatorControl).append(paginator);
				$(paginatorControl).data('paginator',instance);

				// ***** FIN CONSTRUCCION DEL PAGINADOR *****
				
				//se convierte el objeto de datos a renderizar en un arreglo valido
				var arrayData = $.map(data.data, function(value, index) {
				    return [value];
				});

				//RENDEREIZACION DE ITEMS
				//renderizacion del paginador de arriba
				if(config.paginator == "top" || config.paginator == "both"){	
					$(container).before(paginatorControl.clone(true));
				}

				//renderizacion de items
				$(arrayData).each(function(index,element){
					$(container).append(element);
				});

				//renderizacion del paginador de abajo
				if(config.paginator == "bottom" || config.paginator == "both"){	
					$(container).after(paginatorControl.clone(true));
				}

				if(config.appendTo != ''){
					$(config.appendTo).append(paginatorControl.clone(true));
				}

				$(container).fadeIn("300");

				done(null,true)
			});
		};
		
	};

	//SE EXTIENDE EL PLUGIN
	$.fn.ajaxPaginator = function(params,callback){
		return this.each(function(){
			var element = $(this);
			if (element.data('paginator')){
				paginator = element.data('paginator');
			}else{
				var paginator = new Paginator(this,params,callback);
				element.data('paginator', paginator);
				paginator.changePage(1);
			}
			return paginator;
		});
	};

})(jQuery)